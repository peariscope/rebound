(ns rebound.config
  (:require [clojure.java.io :as io]))

(def ^:private config-file (io/resource "config.edn"))

(def config (read-string (slurp config-file)))

(defn port []
  (or (:port config)
      8080))

(defn gemini-port []
  (or (:gemini-port config)
      1965))

(defn domain []
  (or (:domain config)
      "localhost"))
