(ns rebound.server
  (:require [org.httpkit.server :as server]
            [gemclient.core :as gem]
            [gemclient.url :as url]
            [rebound.render :as render]
            [rebound.config :as config]
            [clojure.string :as str]))

(defonce ^:private api-server (atom nil))

(defn- get-path
  "Build the Gemini path that should be called. For input requests
  the query parameters require slight modification."
  [req]
  (let [base (url/absolute (config/domain))
        uri (:uri req)
        query (:query-string req)]
    (if query
      (str base uri (-> query
                        ;; Use Gemini ? query seperator
                        (str/replace "input=" "?")
                        ;; Percent encode spaces
                        (str/replace "+" "%20")))
      (str base uri))))

(defn- handler
  "Make the requested Gemini server call and respond with a
  translated HTTP response."
  [req]
  (let [path (get-path req)
        content (gem/request path :port (config/gemini-port))]
    (println path) ; Log path. Should this be configurable?
    (case (:status content)
      ;; Redirect Temporary
      30 {:status 301
          :headers {"Location" (:meta content)}}
      ;; Redirect Permanent
      31 {:status 301
          :headers {"Location" (:meta content)}}
      ;; Default, render as HTML text
      {:status  200
       :headers {"Content-Type" "text/html; charset=UTF-8"}
       :body    (render/render content)})))

(defn- stop-server
  "Gracefully shutdown the server, waiting 100ms for pending actions."
  []
  (when-not (nil? @api-server)
    (@api-server :timeout 100)
    (reset! api-server nil)))

(defn -main [& args]
  ;; #' enables hot-reloading!
  (reset! api-server
          (server/run-server #'handler {:port (config/port)})))
