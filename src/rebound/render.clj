(ns rebound.render
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [gmi2html.core :as gmi]
            [rebound.config :as config]))

(def ^:private style-file (io/resource "styles.css"))
(def ^:private head-file (io/resource "head.html"))
(def ^:private input-file (io/resource "input.html"))
(def ^:private input-sensitive-file (io/resource "input_sensitive.html"))

(defn- load-styles []
  (slurp style-file))

(defn- load-head []
  (slurp head-file))

(defn- load-input []
  (slurp input-file))

(defn- load-input-senstive []
  (slurp input-sensitive-file))

(defn- get-title-from-first-heading [html]
  ;; Note the ? is required in the regex below to prevent greedy matching
  ;; if multiple header tags are present.
  (let [heading (re-find #"<h[0-3]>(.*?)</h[0-3]>" html)]
    (if (and (vector? heading))
      (second heading)
      nil)))

(defn- build-head-content [html]
  (str
   ;; Set the page title before the pre-configured header content
   ;; so the first heading title overwrites one set in the configuration.
   (let [title (get-title-from-first-heading html)]
     (if title
       (format "<title>%s</title>" title)
       ""))

   ;; Inject configured header content
   (load-head)
   ;; Inject styles
   (format  "<style>%s</style>" (load-styles))))

(defn- add-header [input]
  (format "<head>%s<head>\n<body>%s</body>"
          (build-head-content input)
          input))

(defn- input-resp [prompt]
  (format (load-input) prompt))

(defn- input-sensitive-resp [prompt]
  (format (load-input-senstive) prompt))

(defn render [response]
  (add-header (case (:status response)
                10 (input-resp (:meta response))
                11 (input-sensitive-resp (:meta response))
                20 (gmi/gmi->html (:body response))
                40 (gmi/gmi->html "# 40 TEMPORARY FAILURE")
                41 (gmi/gmi->html "# 41 SERVER UNAVAILABLE")
                42 (gmi/gmi->html (str "# 42 CGI ERROR\n" (:meta response)))
                43 (gmi/gmi->html "# 43 PROXY ERROR")
                44 (gmi/gmi->html "# 44 SLOW DOWN")
                50 (gmi/gmi->html "# 50 PERMANENT FAILURE")
                51 (gmi/gmi->html "# 51 NOT FOUND")
                52 (gmi/gmi->html "# 52 GONE")
                53 (gmi/gmi->html "# 53 PROXY REQUEST REFUSED")
                59 (gmi/gmi->html "# 59 BAD REQUEST")
                60 (gmi/gmi->html "# 60 CLIENT CERTIFICATE REQUIRED")
                61 (gmi/gmi->html "# 61 CERTIFICATE NOT AUTHORISED")
                62 (gmi/gmi->html "# 62 CERTIFICATE NOT VALID")
                (gmi/gmi->html (str "# Unknown Gemini Status: " (:status response))))))


