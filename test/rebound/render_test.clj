(ns rebound.render-test
  (:require [clojure.test :refer :all]
            [clojure.string :as str]
            [rebound.render :as render]))

(defn gmi-resp [body] {:status 20
                       :body body})

(deftest sets-title-default
  (is (true? (str/includes? (render/render (gmi-resp "Hello, no header here."))
                            "<title>Gemini Capsule</title>"))))

(deftest sets-title-from-first-heading-h1
  (is (true? (str/includes? (render/render (gmi-resp "> Hello\n# Test\n# Hello"))
                            "<title>Test</title>"))))

(deftest sets-title-from-first-heading-h2
  (is (true? (str/includes? (render/render (gmi-resp "> Hello\n## Test\n## Hello"))
                            "<title>Test</title>"))))

(deftest sets-title-from-first-heading-h3
  (is (true? (str/includes? (render/render (gmi-resp "> Hello\n### Test\n### Hello"))
                            "<title>Test</title>"))))

(deftest sets-title-from-first-heading
  (is (true? (str/includes? (render/render (gmi-resp "> Hello\n## Test\n# Hello"))
                        "<title>Test</title>"))))




